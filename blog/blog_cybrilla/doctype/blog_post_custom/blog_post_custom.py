# Copyright (c) 2023, Cybrilla and contributors
# For license information, please see license.txt

import frappe
from frappe.website.website_generator import WebsiteGenerator
from frappe.model.document import Document


class BlogPostCustom(WebsiteGenerator):
	# pass
	def after_insert(self):

		authors = self.author
		recipients = []
		for author in authors:
			recipients.append(author.owner)
		
		# frappe.throw(recipients[0])

		frappe.sendmail(
			recipients=recipients,
			subject=frappe._('Blog Post Created'),
			template='email',
			args=dict(
				title="Title",
				message="Message",
			),
			header=('Header')
		)


