from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in blog/__init__.py
from blog import __version__ as version

setup(
	name="blog",
	version=version,
	description="Blog Cybrilla By Ritik",
	author="Cybrilla",
	author_email="ritik.gupta@cyrbilla.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
